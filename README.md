MIQ_PROJECT
******************

_**Case study:**_
Create a VPC (192.168.1.0/24) with 2 subnets  (192.168.1.0/25 and 192.168.1.128/25) in any region. Make one Subnet as Private and other one as Public. Add 2 instances, one in Public and one in Private. Then, create a docker nginx container with ‘HELLO DOCKER’ content in the Private Instance and that docker container should be accessible with a Load Balancer URL.
The Above scenario needs to be automated. Make use of Terraform and Ansible. 

_**Terraform:**_
1. Created the Infrastructure using the Terraform:
    Infra Componenents: [VPC,SUBNET,IGW,NAT,Route Tables,ALB,NAT Instance]
    
   Challenges faced while solving Above scenario:
     When i try to create the loadbalancer it required 2 subnets in Public above CIDR 192.168.1.0/25 and 192.168.1.128/25 occupied 256 ip address on the 192.168.1.0/24. So i changed the Subnet CIDR Private (192.168.1.128/26) and PUBLIC (192.168.1.0/26,192.168.1.64/26)

_**Docker:**_
 1. Create the docker file from nginx image and added Index.html and hello.conf


_**Ansible:**_
 1. Created the sime ansible playbook to Build and deploy the docker image
