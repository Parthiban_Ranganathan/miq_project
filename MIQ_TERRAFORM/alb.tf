############################################
# EC2 Module for external alb servers
############################################
resource "aws_instance" "miq_alb_servers" {
  count                  = var.instance_count_alb
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.miq-private-subnet[count.index].id
  vpc_security_group_ids = [aws_security_group.webserver_alb_sg.id]
  key_name               = var.key_name

  root_block_device {
    volume_type = "gp2"
    volume_size = "20"
  }

      user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname --static "miq-webserver-${count.index+1}"
              echo "127.0.0.1 localhost.localdomain localhost4 localhost4.localdomain4 terraform-alb-servers-${count.index+1} localhost" > hosts
              echo "::1 localhost localhost.localdomain localhost6 localhost6.localdomain6" >> hosts
              EOF

tags = {
    Name                 = "miq-webserver-${count.index+1}"
    terraform            = "true"
  }
}


###############################################
#EC2 SG for ALB
##############################################

resource "aws_security_group" "webserver_alb_sg" {
  name   = "webserver-alb-sg"
  vpc_id = aws_vpc.miq.id

  tags = {
    Name      = "webserver-alb-sg"
    terraform = "true"
  }

    ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.nat_instance_sg.id]
  }

   ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.external_alb_sg.id]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [aws_security_group.external_alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "external_alb_sg" {
  name   = "external-alb-sg"
  vpc_id = aws_vpc.miq.id

  tags = {
    Name      = "external-alb-sg"
    terraform = "true"
  }

   ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

################################################################################
# Deploy External ALB
################################################################################
data "aws_caller_identity" "current" {}

resource "aws_lb" "miq_external_alb" {
  name               = "miq-alb-external"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.external_alb_sg.id]
  subnets            = aws_subnet.miq-public-subnet[*].id

  enable_deletion_protection = true

  tags = {
    Name = "miq-external-alb"
  }
}



#################################################################################
# Target Group & Listener rule
################################################################################

resource "aws_lb_target_group" "external_alb_target_group" {
  name     = "miq-alb-tg"
  port     = "80"
  protocol = "HTTP"
  vpc_id   = aws_vpc.miq.id
  tags = {
    name = "miq-alb-tg"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/"
    port                = "80"
  }
}


resource "aws_alb_listener" "external_alb_listener" {
  load_balancer_arn = aws_lb.miq_external_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.external_alb_target_group.arn
    type             = "forward"
  }
}


###################################################################################
# Target group attachment
##################################################################################

resource "aws_lb_target_group_attachment" "alb_tg_attachment" {
  count            = var.instance_count_alb
  target_group_arn = aws_lb_target_group.external_alb_target_group.arn
  target_id        = element(aws_instance.miq_alb_servers.*.id, count.index)
  port             = 80
}
