################################################################################
# VPC Outputs
################################################################################
output "vpc_id" {
  value = aws_vpc.miq.id
}
output "private_subnets" {
  value = aws_subnet.miq-private-subnet.*.id
}
output "public_subnets" {
  value = aws_subnet.miq-public-subnet.*.id
}



################################################
# EC2 Instance Output
###############################################


output "alb_web_instance_id" {
  value = aws_instance.miq_alb_servers.*.id
}


output "external_alb_dns" {
  value = aws_lb.miq_external_alb.dns_name
}


output "nat_instance_eip" {
  value = aws_eip.nat_eip.*.public_ip
}

output "nat_instance_id" {
  value = aws_instance.nat_instance.*.id
}
