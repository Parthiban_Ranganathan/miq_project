############################################
# EC2 NAT Instance
###########################################
resource "aws_instance" "nat_instance" {
  count                  = var.instance_count_nat
  ami                    = var.nat_ami
  instance_type          = var.nat_instance_type
  subnet_id              = aws_subnet.miq-public-subnet[count.index].id
  vpc_security_group_ids = [aws_security_group.nat_instance_sg.id]
  key_name               = var.key_name

tags = {
    Name                 = "nat-instance-${count.index+1}"
    terraform            = "true"
  }
}


###############################################
#EC2 SG for ALB
##############################################

resource "aws_security_group" "nat_instance_sg" {
  name   = "nat-instance-sg"
  vpc_id = aws_vpc.miq.id

  tags = {
    Name      = "nat-instance-sg"
    terraform = "true"
  }

   ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


######################################################################
#Elastic IP address
######################################################################

resource "aws_eip" "nat_eip" {
  count    = var.instance_count_nat
  instance = aws_instance.nat_instance[count.index].id
  vpc      = true
}
