##################################################################################
# AWS Provider CONFIGURATIONS
##################################################################################
variable "region" {
  description = "Region to deploy infrastructure"
}

##################################################################################
# AWS ALB
##################################################################################


variable "private_subnet" {
  type    = list(string)
  default = []
}

variable "public_subnet" {
  type    = list(string)
  default = []
}


###################################################################################
# EC2 vars
##################################################################################
variable "nat_instance_type" {
  description = "Type of EC2 Instance"
}

variable "instance_count_nat" {
 description = "NAT Instance count"
}

variable "key_name" {
  description = "AWS Keypair"
}


variable "nat_ami" {
  description = "AWS AMI to be used to create the NAT Instance"
}

variable "instance_type" {
  description = "Type of EC2 Instance"
}

variable "instance_count_alb" {
  description = "ALB Instance Count"
}


variable "ami" {
  description = "AWS AMI to be used"
}


