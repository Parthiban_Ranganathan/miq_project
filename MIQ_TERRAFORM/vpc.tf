# VPC MIQ

resource "aws_vpc" "miq" {
	cidr_block 		= "192.168.1.0/24"
	instance_tenancy 	= "default"
	enable_dns_support	= "true"
	enable_dns_hostnames 	= "true"
	enable_classiclink 	= "false"
	tags = {
		Name = "MIQ_VPC"
	}
}

#SUBNETS PRIVATE and PUBLIC

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "miq-public-subnet" {
  count                   = length(var.public_subnet)
  vpc_id                  = aws_vpc.miq.id
  cidr_block              = var.public_subnet[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
		Name = "MIQ_PUBLIC_SUBNET_${count.index+1}"
	}
}


resource "aws_subnet" "miq-private-subnet" {
  count                   = length(var.private_subnet)
  vpc_id                  = aws_vpc.miq.id
  cidr_block              = var.private_subnet[count.index]
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  
  tags = {
		Name = "MIQ_PRIVATE_SUBNET_${count.index+1}"
	}
}



#Internet Gateway
resource "aws_internet_gateway" "miq-gateway" {

	vpc_id = aws_vpc.miq.id
	tags = {
		Name = "MIQ-Internet-Gateway"
	}
}

#route Table

resource "aws_route_table" "miq-public-rt"{
	vpc_id = aws_vpc.miq.id
	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = aws_internet_gateway.miq-gateway.id
	}
	tags = {
		Name = "miq-public-rt"
	}
	
}


#route associations public subnets
resource "aws_route_table_association" "miq-public-rt-assocoation" {
	count                   = length(var.public_subnet)
        subnet_id 		= aws_subnet.miq-public-subnet[count.index].id
	route_table_id 		= aws_route_table.miq-public-rt.id
}



resource "aws_eip" "miq-nat" {
	vpc = true
}

resource "aws_nat_gateway" "miq-nat-gw" {
	allocation_id 	= aws_eip.miq-nat.id
	subnet_id	= aws_subnet.miq-public-subnet[0].id
	depends_on  	= [aws_internet_gateway.miq-gateway]
}

# VPC setup for NAT
resource "aws_route_table" "miq-private-rt" {
  vpc_id = aws_vpc.miq.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.miq-nat-gw.id
  }

  tags = {
    Name = "miq-private-rt"
  }
}

# route associations private
resource "aws_route_table_association" "miq-private-rt-association-1" {
  count                   = length(var.private_subnet)
  subnet_id      = aws_subnet.miq-private-subnet[count.index].id
  route_table_id = aws_route_table.miq-private-rt.id
}


